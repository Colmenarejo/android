package es.bcg.tfm.migraine.core.executor

import es.bcg.tfm.migraine.domain.core.usecase.Executor
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class AndroidExecutor : Executor {

    override val main: CoroutineDispatcher
        get() = Dispatchers.Main
}
