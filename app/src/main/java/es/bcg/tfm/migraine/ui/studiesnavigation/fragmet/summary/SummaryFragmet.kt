package es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.summary

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.fragment.app.Fragment
import es.bcg.tfm.migraine.R
import es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.summary.viewmodel.PatientSummary
import es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.summary.viewmodel.SummaryViewModel
import kotlinx.android.synthetic.main.activity_summary.*

class SummaryFragmet : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_summary, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // TODO remove mock
        var weeks: MutableMap<Int, Int> = mutableMapOf()
        weeks.put(23, 4)
        weeks.put(24, 5)
        weeks.put(25, 7)
        weeks.put(26, 7)

        var patients: MutableList<PatientSummary> = mutableListOf()
        var weeks1: MutableMap<Int, Int> = mutableMapOf()
        weeks1.put(23, 1)
        weeks1.put(24, 1)
        weeks1.put(25, 1)
        weeks1.put(26, 1)
        patients.add(PatientSummary("MG2718006", weeks1))
        var weeks2: MutableMap<Int, Int> = mutableMapOf()
        weeks2.put(23, 1)
        weeks2.put(24, 1)
        weeks2.put(25, 0)
        weeks2.put(26, 0)
        patients.add(PatientSummary("PS2518024", weeks2))
        var weeks3: MutableMap<Int, Int> = mutableMapOf()
        weeks3.put(23, 1)
        weeks3.put(24, 1)
        weeks3.put(25, 0)
        weeks3.put(26, 0)
        patients.add(PatientSummary("PS2518025", weeks3))
        var weeks4: MutableMap<Int, Int> = mutableMapOf()
        weeks4.put(23, 0)
        weeks4.put(24, 0)
        weeks4.put(25, 0)
        weeks4.put(26, 0)
        patients.add(PatientSummary("PS2518026", weeks4))

        var summary = SummaryViewModel("jun2018", weeks, 168, 24, patients)

        createSummary(summary)
        createRoadMap(summary.patients)
    }


    private fun createSummary(summary: SummaryViewModel) {
        var tl = summary_month_info
        // Month
        val tr1 = TableRow(context)
        tr1.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT)

        val textviewTitle = TextView(context)
        textviewTitle.text = "Month"

        val textview = TextView(context)
        textview.text = summary.month

        tr1.addView(textviewTitle)
        tr1.addView(textview)
        tl.addView(
            tr1,
            TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT)
        )

        // Week and patients
        val tr2 = TableRow(context)
        val tr3 = TableRow(context)
        tr2.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT)
        tr3.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT)

        val textviewTitle2 = TextView(context)
        textviewTitle2.text = "Week"
        tr2.addView(textviewTitle2)

        val textviewTitle3 = TextView(context)
        textviewTitle3.text = "Active patients"
        tr3.addView(textviewTitle3)

        for (week: Int in summary.weeks.keys) {
            val textview2 = TextView(context)
            textview2.text = week.toString()
            tr2.addView(textview2)

            val textview3 = TextView(context)
            textview3.text = summary.weeks[week].toString()
            tr3.addView(textview3)
        }

        tl.addView(
            tr2,
            TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT)
        )
        tl.addView(
            tr3,
            TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT)
        )

        // Basal expected
        val tr4 = TableRow(context)
        tr4.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT)

        val textviewTitle4 = TextView(context)
        textviewTitle4.text = "Basal expected"

        val textview4 = TextView(context)
        textview4.text = summary.basalExpected.toString()

        tr4.addView(textviewTitle4)
        tr4.addView(textview4)
        tl.addView(
            tr4,
            TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT)
        )

        // Migraines expected
        val tr5 = TableRow(context)
        tr4.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT)

        val textviewTitle5 = TextView(context)
        textviewTitle5.text = "Migraines expected"

        val textview5 = TextView(context)
        textview5.text = summary.migrainesExpected.toString()

        val textview51 = TextView(context)
        textview51.text = "±"

        val textview52 = TextView(context)
        textview52.text = "6"

        tr5.addView(textviewTitle5)
        tr5.addView(textview5)
        tr5.addView(textview51)
        tr5.addView(textview52)
        tl.addView(
            tr5,
            TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT)
        )
    }

    private fun createRoadMap(patients: List<PatientSummary>) {
        // TODO change this
        val tl = summary_patients_info

        // Road map
        for (patient : PatientSummary in patients) {

            val tr = TableRow(context)
            tr.layoutParams =
                TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT)

            val textviewTitle = TextView(context)
            textviewTitle.text = patient.name
            tr.addView(textviewTitle)

            for (week: Int in patient.weeks.keys) {
                val textview = TextView(context)
                if (patient.weeks[week] == 1){
                    textview.setBackgroundColor(Color.rgb(153, 204, 0))
                } else {
                    textview.setBackgroundColor(Color.rgb(153, 51, 51))
                }
                tr.addView(textview)
            }

            tl.addView(
                tr,
                TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT)
            )
        }
    }

}
