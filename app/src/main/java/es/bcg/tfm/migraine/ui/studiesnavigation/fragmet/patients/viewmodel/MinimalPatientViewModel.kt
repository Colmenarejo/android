package es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients.viewmodel

data class MinimalPatientViewModel(

    val id: String,

    val email: String,

    val name: String,

    val startDay: String,

    val sex: String

)