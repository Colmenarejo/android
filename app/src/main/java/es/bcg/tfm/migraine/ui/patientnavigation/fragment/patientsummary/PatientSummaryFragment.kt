package es.bcg.tfm.migraine.ui.patientnavigation.fragment.patientsummary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import es.bcg.tfm.migraine.R
import es.bcg.tfm.migraine.ui.patientnavigation.fragment.patientsummary.viewmodel.PatientSummaryViewModel
import kotlinx.android.synthetic.main.activity_migraine.*
import kotlinx.android.synthetic.main.item_patient.*

class PatientSummaryFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_migraine, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        patient_icon_sex.setImageResource(R.drawable.ic_female)
        patient_edit.visibility = View.GONE
        patient_name.text = "Vanesa Calvo Broncano"
        patient_date.text = "vanesaclv@gmail.com"
        patient_init_date.text = "2018/05/05"

        val migraine = PatientSummaryViewModel("5/10", "3/10", "1/10", "1/10", "1/10", "22")

        total_migraines_value.text = migraine.total
        migraines_to_predict_value.text = migraine.toPredict
        falses_migraines_value.text = migraine.falses
        migraines_not_monitorized_value.text = migraine.notMonitorized
        migraines_with_noise_value.text = migraine.withNoise
        basal_days_value.text = migraine.basalDays
    }

}
