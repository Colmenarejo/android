package es.bcg.tfm.migraine.ui.login

import es.bcg.tfm.migraine.domain.core.exception.FailureType
import es.bcg.tfm.migraine.domain.model.user.UserModel
import es.bcg.tfm.migraine.presentation.features.login.LoginPresenter
import es.bcg.tfm.migraine.presentation.features.login.LoginViewInteractor
import es.bcg.tfm.migraine.ui.login.viewmodel.UserViewModel

class LoginViewInteractorImpl(
    private val view: LoginContractView,
    private val presenter: LoginPresenter
) : LoginViewInteractor {

    override fun initialize() {
    }

    override fun init() {
        presenter.attachInteractor(this)
        presenter.initialize()
    }

    override fun loginUser(userId: String, password: String) {
        presenter.loginUser(userId, password)
    }

    override fun showLoginUserSuccess(userModel: UserModel) {
        view.showLoginOk(UserViewModel.transform(userModel))
    }

    override fun showLoginUserError(error: FailureType) {
        view.showLoginError()
    }

    override fun destroy() {
        presenter.destroy()
    }

    override fun showError(error: FailureType) {
    }

    override fun logOut() {
    }
}