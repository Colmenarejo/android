package es.bcg.tfm.migraine.ui.patientnavigation.fragment.patientdetail

import es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients.viewmodel.PatientViewModel

interface PatientContractView {

    fun showPatientOk(patientViewModel: PatientViewModel)

    fun showPatientError()

}