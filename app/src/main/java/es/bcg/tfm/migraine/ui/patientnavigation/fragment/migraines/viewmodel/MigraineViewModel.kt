package es.bcg.tfm.migraine.ui.patientnavigation.fragment.migraines.viewmodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MigraineViewModel (

    val id: Int,

    val initDate: String,

    val endDate: String,

    val status : String,

    val cure : String,

    val painIntensity: Int,

    val painEvol: Int,

    val premonitory: String,

    val medicines : String,

    val symptom : String

) : Parcelable