package es.bcg.tfm.migraine.ui.signup

interface SignupContractView {

    fun showSignupOk()

    fun showSignupError()

}