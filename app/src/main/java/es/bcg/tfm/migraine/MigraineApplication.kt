package es.bcg.tfm.migraine

import android.app.Application
import es.bcg.tfm.migraine.data.local.LocalData
import es.bcg.tfm.migraine.data.local.LocalDataImpl

class MigraineApplication : Application() {

    companion object {
        val localData: LocalData = LocalDataImpl()
    }
}