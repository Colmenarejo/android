package es.bcg.tfm.migraine.ui.patientnavigation.fragment.migraines

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import es.bcg.tfm.migraine.R
import es.bcg.tfm.migraine.ui.patientnavigation.PatientNavigationActivity
import es.bcg.tfm.migraine.ui.patientnavigation.fragment.migraines.viewmodel.MigraineViewModel
import kotlinx.android.synthetic.main.activity_road_map.*
import java.util.*

class MigrainesFragment : Fragment() {

    private val GOOD_R : Int = 149
    private val GOOD_G : Int = 221
    private val GOOD_B : Int = 0

    private val FALSE_R : Int = 255
    private val FALSE_G : Int = 153
    private val FALSE_B : Int = 0

    private val BAD_R : Int = 149
    private val BAD_G : Int = 0
    private val BAD_B : Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_road_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var migraines : MutableList<MigraineViewModel> = mutableListOf()

        migraines.add(MigraineViewModel(1, "2019/01/23", "06:10:43", "GOOD", "No se produjo aura", 6, 16, "Bostezos, sed", "AINE, AINE", "Frio, Nauseas"))
        migraines.add(MigraineViewModel(2, "2019/01/27", "08:17:58", "FALSE", "No se produjo aura", 6, 16, "Bostezos, sed", "AINE, AINE", "Frio, Nauseas"))
        migraines.add(MigraineViewModel(3, "2019/02/03", "13:27:12",  "BAD", "No se produjo aura", 6, 16, "Bostezos, sed", "AINE, AINE", "Frio, Nauseas"))
        migraines.add(MigraineViewModel(4, "2019/02/13", "07:11:33",  "BAD", "No se produjo aura", 6, 16, "Bostezos, sed", "AINE, AINE", "Frio, Nauseas"))
        migraines.add(MigraineViewModel(5, "2019/03/10", "10:04:19",  "FALSE", "No se produjo aura", 6, 16, "Bostezos, sed", "AINE, AINE", "Frio, Nauseas"))
        migraines.add(MigraineViewModel(6, "2019/01/23", "06:10:43", "GOOD", "No se produjo aura", 6, 16, "Bostezos, sed", "AINE, AINE", "Frio, Nauseas"))
        migraines.add(MigraineViewModel(7, "2019/01/23", "06:10:43", "BAD", "No se produjo aura", 6, 16, "Bostezos, sed", "AINE, AINE", "Frio, Nauseas"))

        list_migraines.adapter = MigraineArrayAdapter(this.context!!, migraines)

        list_migraines.onItemClickListener = AdapterView.OnItemClickListener()
        { parent, view, position, id ->
            val intent = Intent(context, MigraineDetailActivity::class.java)
            var bundle = Bundle()
            bundle.putParcelable("migraine", migraines.get(position))
            intent.putExtra("bundleMigraine",bundle)
            startActivity(intent)
        }
    }

    private inner class MigraineArrayAdapter : ArrayAdapter<MigraineViewModel> {

        internal var items: List<MigraineViewModel>

        constructor(context: Context, objects: List<MigraineViewModel>)
                : super(context, R.layout.item_migraine, objects) {
            this.items = objects
        }

        private inner class ViewHolder(
            var icon: ImageView? = null,
            var id: TextView? = null,
            var initDate: TextView? = null,
            var endDate : TextView? = null
        )

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var view: View?
            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.item_migraine, null)
                val viewHolder = ViewHolder()
                viewHolder.icon = view.findViewById(R.id.migraine_icon) as ImageView
                viewHolder.id = view.findViewById(R.id.migraine_id) as TextView
                viewHolder.initDate = view.findViewById(R.id.migraine_init_date) as TextView
                viewHolder.endDate = view.findViewById(R.id.migraine_end_date) as TextView
                view.tag = viewHolder
            } else {
                view = convertView
                (view.tag as ViewHolder)
            }
            val holder = view!!.tag as ViewHolder

            holder.id!!.text = "Migraine ${items[position].id.toString()}"

            if (items[position].status == "GOOD") {
                holder.icon!!.setColorFilter(Color.rgb(GOOD_R, GOOD_G, GOOD_B))
            } else if (items[position].status == "FALSE"){
                holder.icon!!.setColorFilter(Color.rgb(FALSE_R, FALSE_G, FALSE_B))
            } else {
                holder.icon!!.setColorFilter(Color.rgb(BAD_R, BAD_G, BAD_B))
            }
            holder.initDate!!.text = items[position].initDate.toString()
            holder.endDate!!.text = items[position].endDate.toString()

            return view
        }

    }

}
